/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package absa_assessment;

import java.util.List;
import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author Lionel Mangoua 
 * Date: 13/01/18 
 * Description: Functional UI Test
 */

public class AbsaAssessment {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {

        //Set Property of ChromeDriver
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\C55-C1881 White\\Documents\\Softwares\\SeleniumDrivers\\chromedriver_win32\\chromedriver.exe");

        //Setup Driver
        WebDriver driver = new ChromeDriver();

        //Open URL
        driver.get("http://www.way2automation.com/angularjs-protractor/webtables/");

        //Maximize window
//        driver.manage().window().maximize();

        //Validate User List Table
        /*Validate presence of "Add User" button*/
        WebElement addUser;

        addUser = driver.findElement(By.xpath("//button[@type= 'add']"));
        Assert.assertEquals(true, addUser.isDisplayed());

        //Click "Add User" button 
        addUser.click();

        Thread.sleep(2000);

        //Add user 1 
        /*Enter "First Name" */
        WebElement fName;

        fName = driver.findElement(By.xpath("//input[@name='FirstName']"));
        
        fName.clear();

        fName.sendKeys("FName1");

        Thread.sleep(2000);

        /*Enter "Last Name" */
        WebElement lName;

        lName = driver.findElement(By.xpath("//input[@name='LastName']"));
        
        lName.clear();

        lName.sendKeys("LName1");

        Thread.sleep(2000);

        /*Enter "User Name" */
        WebElement uName;

        uName = driver.findElement(By.xpath("//input[@name='UserName']"));
        
        uName.clear();

        uName.sendKeys("User1");

        Thread.sleep(2000);

        /*Enter "Password" */
        WebElement pwd;

        pwd = driver.findElement(By.xpath("//input[@name='Password']"));
        
        pwd.clear();

        pwd.sendKeys("Pass1");

        Thread.sleep(2000);

        /*click "Customer AAA" */
        WebElement cust;

        cust = driver.findElement(By.xpath("//input[@value='15']"));

        cust.click();

        Thread.sleep(2000);

        /*Select "Role" */
        Select dropdown = new Select(driver.findElement(By.xpath("//select[@name='RoleId']")));
        dropdown.selectByVisibleText("Admin");

        Thread.sleep(2000);

        /*Enter "Email" */
        WebElement email;

        email = driver.findElement(By.xpath("//input[@name='Email']"));
        
        email.clear();

        email.sendKeys("admin@mail.com");

        Thread.sleep(2000);

        /*Enter "Cell" */
        WebElement cel;

        cel = driver.findElement(By.xpath("//input[@name='Mobilephone']"));
        
        cel.clear();

        cel.sendKeys("082555");

        Thread.sleep(2000);

        /*Click "Save" btn */
        WebElement save;

        save = driver.findElement(By.xpath("//button[contains(text(),'Save')]"));

        save.click();
 
        Thread.sleep(2000);

        //Click "Add User" button 
        addUser.click();

        Thread.sleep(2000);

        //Add user 2 
        /*Enter "First Name" */
        fName = driver.findElement(By.xpath("//input[@name='FirstName']"));

        fName.clear();

        fName.sendKeys("FName2");

        Thread.sleep(2000);

        /*Enter "Last Name" */
        lName = driver.findElement(By.xpath("//input[@name='LastName']"));

        lName.clear();

        lName.sendKeys("LName2");

        Thread.sleep(2000);

        /*Enter "User Name" */
        WebElement uName2;
        
        uName2 = driver.findElement(By.xpath("//input[@name='UserName']"));

        uName2.clear();

        uName2.sendKeys("User2");

        Thread.sleep(2000);

        /*Enter "Password" */
        pwd = driver.findElement(By.xpath("//input[@name='Password']"));

        pwd.clear();

        pwd.sendKeys("Pass2");

        Thread.sleep(2000);

        /*click "Customer BBB" */
        cust = driver.findElement(By.xpath("//input[@value='16']"));

        cust.click();

        Thread.sleep(2000);

        /*Select "Role" */
        Select dropdown2 = new Select(driver.findElement(By.xpath("//select[@name='RoleId']")));
        dropdown2.selectByVisibleText("Customer");

        Thread.sleep(2000);

        /*Enter "Email" */
        email = driver.findElement(By.xpath("//input[@name='Email']"));

        email.clear();

        email.sendKeys("customer@mail.com");

        Thread.sleep(2000);

        /*Enter "Cell" */
        cel = driver.findElement(By.xpath("//input[@name='Mobilephone']"));

        cel.clear();

        cel.sendKeys("083444");

        Thread.sleep(2000);

        /*click "Save" btn */
        save = driver.findElement(By.xpath("//button[contains(text(),'Save')]"));

        save.click();
        
        Thread.sleep(2000);

        /*Verify users created are in the list*/
        
        //Grab the table  
        WebElement table = driver.findElement(By.xpath("//table/tbody"));

        //Now get all the TR elements from the table 
        List<WebElement> allRows = table.findElements(By.tagName("tr"));

        //And iterate over them, getting the cells 
        for (WebElement row : allRows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));

            //Print the contents of each cell
            for (WebElement cell : cells) { 
                if (cell.getText().contentEquals("User1")) {
                    System.out.println("");
                    System.out.println(cell.getText() + " is in the list.");
                    System.out.println("********************************");
                }
                else if (cell.getText().contentEquals("User2")) {
                    System.out.println("");
                    System.out.println(cell.getText() + " is in the list.");
                    System.out.println("********************************");
                } 
            }
        }

        //Close browser
        driver.close();

    }

}
